import React from "react";
import "./App.css";
import web3 from "./web3";
import contract from "./myerc20";

class App extends React.Component {
  state = {
    address: "",
    balance: "",
    value: "",
    message: "",
  };

  buyCoin = async (event) => {
    event.preventDefault();

    const accounts = await web3.eth.getAccounts();

    this.setState({ message: "Waiting on transaction success..." });

    await contract.methods.buy().send({
      from: accounts[0],
      value: web3.utils.toWei(this.state.value, "ether"),
    });

    this.setState({ message: "You have been entered! " });
  };

  async componentDidMount() {
    const address = contract.options.address;
    const balance = await web3.eth.getBalance(contract.options.address);
    this.setState({ address });
    this.setState({ balance });
  }

  render() {
    return (
      <div className="App">
        <h1>DIT Token</h1>
        <h2>{this.state.address}</h2>
        <form onSubmit={this.buyCoin}>
          <h1>Amount of ETH to buy</h1>
          <input
            type="number"
            value={this.state.value}
            onChange={(event) => this.setState({ value: event.target.value })}
          />
          <input type="submit" value="Buy" />
        </form>

        <style>{"table{ margin-left: auto; margin-right: auto;}"}</style>
        <table>
          <tr>
            <td><h1>PRICE</h1></td>
            <td><h1>TOTAL SUPPLY</h1></td>
          </tr>
          <tr>
            <td><h2>1 ETH = 10 DIT</h2></td>
            <td><h2>1,000,000 DIT</h2></td>
          </tr>
        </table>
        <h2>TOTAL RAISED</h2>
        <h2>{web3.utils.fromWei(this.state.balance, "ether")} ETH</h2>

        <h1>{this.state.message}</h1>
      </div>
    );
  }
}

export default App;
